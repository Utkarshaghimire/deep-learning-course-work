# Face Mask Recognition - Deep Learning Assignment 


Following are the requirements for running the Face mask recognition model on the system:

1.                 Jupyter Notebook version (anaconda3) should be installed. (eg: C:\User\Folder1)
2.                 The following list of libraries should be installed as well upon the completion of Step 1 above:
                                     tensorflow>=2.3.0
                                     keras=2.4.3
                                     imutils=0.5.4
                                     numpy=1.20.1
                                     opencv-python=4.0.1
                                     matplotlib=3.3.4
                                     scipy=1.6.23.
                   All files, images and datasets are to be stored in the one same folder.
3.                 The provided files on GitLab to be moved to the same folder (eg: C:\User\Folder1)
4.                 The files on GitLab to be extracted under the same folder as mentioned in the step above. 
                   (eg: C:\User\Folder1) 
5.                 On completion of the above step i.e. extraction of the file content user should see the following file under the same directory (eg: C:\User\Folder1)
                   Deep Learning - Coursework.ipynb
                   Deep Learning - Transfer Learning.ipynb 
6.                 To run the code follow the instruction below:
                   Create an empty folder in (eg: C:\User\Folder1) as MODEL to save the generated files in it
                                     Open Jupyter Notebook
                                     Open Deep Learning - Coursework.ipynb File and run the cells of the file one by one.
                                     Open Deep Learning - Transfer Learning.ipynb File and run the cells of the file one by one.
7.                 Upon successfully executing the code, the user should see the following files in the same folder (eg: C:\User\Folder1)\MODEL
                                     MODEL/FINAL.npy 
                                     MODEL/IMAGES.npy 
                                     (eg: C:\User\Folder1)/recognition.h5 
                                     (eg: C:\User\Folder1)/newmobilenet_model.h5 
